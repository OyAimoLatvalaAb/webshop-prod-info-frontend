/* eslint-env node */
const path = require('path');
const DefinePlugin = require('webpack').DefinePlugin;
const dotenv = require('dotenv');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: 'src/index.html',
    filename: 'index.html',
    title: 'WebShop JSON Formatter',
    inject: 'body'
});

const pack = require('./package.json');
const appName = pack.name;
const appVersion = pack.version;
// const buildNumber = process.env.BUILD_NUMBER || 'local_dev';
const WebpackShellPluginNext = require('webpack-shell-plugin-next');
const buildsDir = path.resolve(__dirname, 'builds');
const distDir = path.resolve(__dirname, 'dist');
const ENSURE_BUILDS_DIR = 'test -d '+buildsDir+' || mkdir '+buildsDir;
const ENSURE_DIST_DIR = 'test -d '+distDir+' || mkdir '+distDir;
const MERGE_ASSET_COMPILE = '[ -d assets ] && cp -R '+path.join('assets', '*')+' "'+distDir+'" || true';
const archiveName = appName+'-'+appVersion+'.tar.gz';
const archivePath = path.join(buildsDir, archiveName);
const CREATE_ARCHIVE = `uname -a | grep -iqv cygwin && tar czvf ${archivePath} -C ${distDir} . && echo "Built ${archiveName}" || echo "skipping archive creation, unsupported platform"`;


module.exports = () => {
    let env = dotenv.config().parsed;
    if(typeof env === 'undefined') {
        console.warn("Couldn't load .env, falling back to defaults")
        env = {
            DEFAULT_JWT_TOKEN: '',
            API_URL: '/api/',
        };
    }
    const envKeys = Object.keys(env).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(env[next]);
        return prev;
    }, {});
    return {
        entry: './src/index.js',
        resolve: {
            extensions: ['.js'],
        },
        module: {
            rules: [
                {
                    test: /\.jsx$|\.js$/,
                    enforce: 'pre',
                    loader: 'eslint-loader',
                    include: __dirname+'/src',
                    exclude: __dirname+'/node_modules',
                },
                {
                    test: /\.jsx$|\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader?modules'],
                },
                {
                    test: /\.(png|jpe?g|gif)$/i,
                    loader: 'url-loader?limit=100000',
                },
                {
                    test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'url-loader?limit=10000&mimetype=application/font-woff',
                },
                {
                    test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'url-loader?limit=10000&mimetype=application/octet-stream',
                },
                {
                    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'file-loader',
                },
                {
                    test: /\.otf$/,
                    loader: 'file-loader',
                },
                {
                    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                    loader: 'url-loader?limit=10000&mimetype=image/svg+xml',
                },
            ],
        },
        plugins: [
            HtmlWebpackPluginConfig,
            new WebpackShellPluginNext({
                onBuildStart: {
                    scripts: [
                        ENSURE_BUILDS_DIR,
                        ENSURE_DIST_DIR,
                    ],
                    blocking: true,
                },
                onBuildExit: {
                    scripts: [
                        MERGE_ASSET_COMPILE,
                        CREATE_ARCHIVE,
                    ],
                    blocking: true,
                },
                safe: true,
            }),
            new DefinePlugin(envKeys),
        ],
        output: {
            path: path.resolve(__dirname, 'dist'),
            publicPath: '/',
            filename: 'bundle.js',
        },
    }
};
