const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

let allowRemoteDev = true;

module.exports = (env) => {
    const conf = merge(common(env), {
        devServer: {
            contentBase: [
                path.resolve(__dirname, 'assets'),
            ],
            proxy: {
                '/api': {
                    target: 'http://localhost:3000',
                    pathRewrite: {'^/api' : ''}
                }
            },
            compress: true,
            host: allowRemoteDev ? '0.0.0.0' : 'localhost',
            port: 3002,
            disableHostCheck: allowRemoteDev
        },
        devtool: "cheap-module-source-map",
        mode: 'development'
    });
    return conf;
}
