const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = () => {
    return merge(common(), {
        plugins: [
            new UglifyJSPlugin()
        ],
        mode: 'production'
    });
}
