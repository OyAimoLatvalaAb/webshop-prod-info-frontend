// ENUM in backend
export const CATALOGVISIBILITY = {
    visible: 'visible',
    catalog: 'catalog',
    search: 'search',
    hidden: 'hidden'
};
export const MAX_JSON_LENGTH = 4096
export const HTTP_NOT_FOUND = 404
export const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: `${theme.spacing(3)}px 0 ${theme.spacing(5)}px`,
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(),
    },
    leftIcon: {
        marginRight: theme.spacing(),
    },
    rightIcon: {
        marginLeft: theme.spacing(),
    },
});
