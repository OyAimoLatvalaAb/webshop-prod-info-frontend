import {
    CLEAR_PRODUCT_INPUT_FORM,
    SET_CATALOG_VISIBILITY,
    SET_DIM_H_CM,
    SET_DIM_W_CM,
    SET_DIM_L_CM,
    SET_MANUFACTURER_WEB_URL,
    SET_MASS_G,
    SET_PROD_TEXT,
    SET_SALE_PRICE,
    SET_UNIT,
    SET_EAN,
    SET_PRODUCT_EAN,
    LOAD_PRODUCT_INFO,
    SAVE_PRODUCT_INFO,
    SET_JWT,
    SET_IS_LARGE,
    SET_IS_DANGEROUS,
    SET_IS_FRAGILE,
    SET_VAK_AMOUNT,
    REQUEST_CLEAR_INPUT_FORM,
} from '../constants/action-types'

export const onRequestClearInputForm = () => {
    return {
        type: REQUEST_CLEAR_INPUT_FORM,
    };
}

export const onClearInputForm = () => {
    return {
        type: CLEAR_PRODUCT_INPUT_FORM,
    }
}

export const onChangeCatalogVisibility = (catalogVisibility) => {
    return {
        type: SET_CATALOG_VISIBILITY,
        payload: catalogVisibility,
    }
}

export const onChangeDimHCM = (dimHCM) => {
    return {
        type: SET_DIM_H_CM,
        payload: dimHCM,
    }
}

export const onChangeDimWCM = (dimWCM) => {
    return {
        type: SET_DIM_W_CM,
        payload: dimWCM,
    }
}

export const onChangeDimLCM = (dimLCM) => {
    return {
        type: SET_DIM_L_CM,
        payload: dimLCM,
    }
}

export const onChangeManufacturerWebUrl = (manufacturerWebUrl) => {
    return {
        type: SET_MANUFACTURER_WEB_URL,
        payload: manufacturerWebUrl,
    }
}

export const onChangeMassG = (massG) => {
    return {
        type: SET_MASS_G,
        payload: massG,
    }
}

export const onChangeProdText = (text) => {
    return {
        type: SET_PROD_TEXT,
        payload: text,
    }
}

export const onChangeSalePrice = (salePrice) => {
    return {
        type: SET_SALE_PRICE,
        payload: salePrice,
    }
}

export const onChangeUnit = (unit) => {
    return {
        type: SET_UNIT,
        payload: unit
    }
}

export const onChangeEan = (ean) => {
    return {
        type: SET_EAN,
        payload: ean
    }
}

export const onChangeIsLarge = (isLarge) => {
    return {
        type: SET_IS_LARGE,
        payload: isLarge,
    }
}

export const onChangeIsDangerous = (isDangerous) => {
    return {
        type: SET_IS_DANGEROUS,
        payload: isDangerous,
    }
}

export const onChangeIsFragile = (isFragile) => {
    return {
        type: SET_IS_FRAGILE,
        payload: isFragile,
    }
}

export const onChangeVakAmount = (vakAmount) => {
    return {
        type: SET_VAK_AMOUNT,
        payload: vakAmount
    }
}

export const onLoadProductInfo = () => {
    return {
        type: LOAD_PRODUCT_INFO,
    }
}

export const onSaveProductInfo = () => {
    return {
        type: SAVE_PRODUCT_INFO,
    }
}

export const onChangeActiveProductEan = (ean) => {
    return {
        type: SET_PRODUCT_EAN,
        payload: ean,
    }
}

export const onChangeJWT = (jwt) => {
    return {
        type: SET_JWT,
        payload: {
            JWT: jwt,
        }
    }
}
