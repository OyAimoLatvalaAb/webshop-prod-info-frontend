import { createStore, applyMiddleware, } from "redux";
import { createLogicMiddleware } from 'redux-logic'

import businessLogic from '../logic'

import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';

import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from "../reducers/index";

const composedMiddleware = composeWithDevTools(
    applyMiddleware(
        thunkMiddleware,
        promiseMiddleware,
        createLogicMiddleware(businessLogic),
    )
)

const store = createStore(
    rootReducer,
    composedMiddleware
);
export default store;
