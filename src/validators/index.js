const isEan = (str) => {
    const rgx = /^[0-9]{3,13}$/
    return rgx.test(str)
}

export {
    isEan
};
