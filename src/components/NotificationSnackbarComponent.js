import React from 'react';
import { Snackbar } from '@material-ui/core'
import PropTypes from 'prop-types'

class NotificationSnackbarComponent extends React.Component {

    static propTypes = {
        open: PropTypes.bool,
        message: PropTypes.string
    }

    render() {
        return (
            <React.Fragment>
                <Snackbar
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right'}}
                    open={typeof this.props.open === 'boolean' ? this.props.open : true}
                    message={this.props.message}
                />
            </React.Fragment>
        )
    }
}

export default NotificationSnackbarComponent
