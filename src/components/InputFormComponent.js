import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';
import { CATALOGVISIBILITY } from '../constants/';
import WysiwygEditor from './WysiwygEditor';
import {FormGroup, FormControlLabel, Checkbox} from '@material-ui/core';

class InputFormComponent extends React.Component {
    static propTypes = {
        ean: PropTypes.string,
        salePrice: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        dimWCM: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        dimHCM: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        dimLCM: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        manufacturerWebUrl: PropTypes.string,
        unit: PropTypes.string,
        massG:  PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string,
        ]),
        text: PropTypes.string,
        catalogVisibility: PropTypes.oneOf(
            Object.getOwnPropertyNames(CATALOGVISIBILITY).map(
                (k) => CATALOGVISIBILITY[k]
            )
        ),
        onChangeSalePrice: PropTypes.func.isRequired,
        onChangeDimWCM: PropTypes.func.isRequired,
        onChangeDimHCM: PropTypes.func.isRequired,
        onChangeDimLCM: PropTypes.func.isRequired,
        onChangeManufacturerWebUrl: PropTypes.func.isRequired,
        onChangeUnit: PropTypes.func.isRequired,
        onChangeMassG: PropTypes.func.isRequired,
        onChangeProdText: PropTypes.func.isRequired,
        onChangeCatalogVisibility: PropTypes.func.isRequired,
        isNewProduct: PropTypes.bool.isRequired,
        productIsLoading: PropTypes.bool.isRequired,
        isBusy: PropTypes.bool.isRequired,
        isLarge: PropTypes.bool.isRequired,
        onChangeIsLarge: PropTypes.func.isRequired,
        isDangerous: PropTypes.bool.isRequired,
        onChangeIsDangerous: PropTypes.func.isRequired,
        isFragile: PropTypes.bool.isRequired,
        onChangeIsFragile: PropTypes.func.isRequired,
        vakAmount: PropTypes.number.isRequired,
        onChangeVakAmount: PropTypes.func.isRequired,
    }
    render() {
        return (
            <React.Fragment>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="salePrice"
                            name="salePrice"
                            label="Ale-hinta"
                            fullWidth
                            value={this.props.salePrice}
                            onChange={this.props.onChangeSalePrice}
                            disabled={this.props.isBusy}
                        />
                        <FormHelperText>Jätä tyhjäksi, jollei ole alennusta</FormHelperText>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            required
                            id="dimWCM"
                            name="dimWCM"
                            label="Leveys (cm)"
                            fullWidth
                            value={this.props.dimWCM}
                            onChange={this.props.onChangeDimWCM}
                            disabled={this.props.isBusy}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            required
                            id="dimLCM"
                            name="dimLCM"
                            label="Pituus (cm)"
                            fullWidth
                            value={this.props.dimLCM}
                            onChange={this.props.onChangeDimLCM}
                            disabled={this.props.isBusy}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            required
                            id="dimHCM"
                            name="dimHCM"
                            label="Korkeus (cm)"
                            fullWidth
                            value={this.props.dimHCM}
                            onChange={this.props.onChangeDimHCM}
                            disabled={this.props.isBusy}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="manufacturerWebUrl"
                            name="manufacturerWebUrl"
                            label="Tuotteen Web URL"
                            fullWidth
                            value={this.props.manufacturerWebUrl}
                            onChange={this.props.onChangeManufacturerWebUrl}
                            disabled={this.props.isBusy}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="unit"
                            name="unit"
                            label="Yksikkö"
                            fullWidth
                            value={this.props.unit}
                            onChange={this.props.onChangeUnit}
                            disabled={this.props.isBusy}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="massG"
                            name="massG"
                            label="Paino (g)"
                            fullWidth
                            value={this.props.massG}
                            onChange={this.props.onChangeMassG}
                            disabled={this.props.isBusy}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="vakAmount"
                            name="vakAmount"
                            label="VAK määrä"
                            fullWidth
                            value={this.props.vakAmount}
                            onChange={this.props.onChangeVakAmount}
                            disabled={this.props.isBusy}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormGroup row>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.props.isLarge}
                                        onChange={this.props.onChangeIsLarge}
                                        disabled={this.props.isBusy}
                                    />
                                }
                                label="Iso"
                            />
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.props.isDangerous}
                                        onChange={this.props.onChangeIsDangerous}
                                        disabled={this.props.isBusy}
                                    />
                                }
                                label="Vaarallinen"
                            />
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.props.isFragile}
                                        onChange={this.props.onChangeIsFragile}
                                        disabled={this.props.isBusy}
                                    />
                                }
                                label="Särkyvä"
                            />
                        </FormGroup>
                    </Grid>
                    <Grid item xs={12}>
                        <WysiwygEditor
                            setProductText={this.props.onChangeProdText}
                            html={this.props.text}
                            readOnly={this.props.isBusy}
                            placeholder='Tuotteen kuvaus'
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <InputLabel htmlFor="inputCatVis">Näkyvyys</InputLabel>
                        <Select
                            id="catalogVisibility"
                            name="catalogVisibility"
                            input={<Input name="inputCatVis" id="inputCatVis" />}
                            fullWidth
                            value={this.props.catalogVisibility}
                            onChange={this.props.onChangeCatalogVisibility}
                            disabled={this.props.isBusy}
                        >
                            {
                                Object.getOwnPropertyNames(CATALOGVISIBILITY).map((k) =>
                                    CATALOGVISIBILITY[k]
                                ).map((val) => (
                                    <MenuItem key={'catVis'+val} value={val}>{val}</MenuItem>
                                ))
                            }
                        </Select>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

export default InputFormComponent;
