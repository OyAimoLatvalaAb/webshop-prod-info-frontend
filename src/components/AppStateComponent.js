import React, {Component} from "react";
import {string} from "prop-types";
import {Typography, Grid} from "@material-ui/core";

class AppStateComponent extends Component {
    static propTypes = {
        statusMsg: string.isRequired,
    }
    render() {
        return (
            <Grid item xs={12}>
                <Typography component="h1" variant="h6" style={{ marginTop: '12px' }} align="center">
                    {
                        this.props.statusMsg
                    }
                </Typography>
            </Grid>
        )
    }
}

export default AppStateComponent;
