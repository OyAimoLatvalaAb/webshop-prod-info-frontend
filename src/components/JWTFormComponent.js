import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';

class JWTFormComponent extends React.Component {
    static propTypes = {
        JWT: PropTypes.string,
        onChangeJWT: PropTypes.func.isRequired,
    }
    render() {
        return (
            <React.Fragment>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="jwt"
                            name="jwt"
                            label="Tunnus"
                            fullWidth
                            value={this.props.JWT || ''}
                            onChange={this.props.onChangeJWT}
                            type="password"
                        />
                        <FormHelperText>Tämän saat ylläpidolta</FormHelperText>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

export default JWTFormComponent;
