import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import {
    CssBaseline,
    AppBar,
    Toolbar,
    Paper,
    Typography,
} from '@material-ui/core'
import InputFormContainer from '../containers/InputFormContainer';
import ProductSelectionContainer from '../containers/ProductSelectionContainer'
import NotificationSnackbarComponent from '../components/NotificationSnackbarComponent'
import ProductInfoProps from '../models/ProductInfoProps';
import ActionButtonsContainer from '../containers/ActionButtonsContainer';
import {styles} from '../constants';
import AppStateContainer from '../containers/AppStateContainer';
import JWTFormContainer from '../containers/JWTFormContainer';



class App extends Component {

    static propTypes = {
        classes: PropTypes.object.isRequired,
        ean: PropTypes.string,
        productIsLoading: PropTypes.bool.isRequired,
        productInfo: ProductInfoProps,
        notificationStack: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.any.isRequired,
            open: PropTypes.bool,
            message: PropTypes.string.isRequired,
        })).isRequired,
    };

    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <CssBaseline />
                <AppBar position="absolute" color="default" className={classes.appBar}>
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap>
                            WebShop Product Info
                        </Typography>
                    </Toolbar>
                </AppBar>
                <main className={classes.layout}>
                    {
                        this.props.notificationStack.map((el) => {
                            return (<NotificationSnackbarComponent
                                key={el.id}
                                open={el.open}
                                message={el.message}
                            />)
                        })
                    }
                    <Paper className={classes.paper}>
                        <Typography component="h1" variant="h4" align="center">
                            Webshop Product Info
                        </Typography>
                        <React.Fragment>
                            <ProductSelectionContainer />
                            <AppStateContainer />
                            {
                                this.props.productInfo !== null ? (<InputFormContainer />) : null
                            }
                            <ActionButtonsContainer />
                            <JWTFormContainer />
                        </React.Fragment>
                    </Paper>
                </main>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(App);
