import React, { Component } from 'react';
import { func, string, bool } from 'prop-types';
import { TextField } from '@material-ui/core';


export default class ProductSelectionComponent extends Component {

    static propTypes = {
        ean: string.isRequired,
        onChangeActiveProductEan: func.isRequired,
        onEanFieldKeyUp: func.isRequired,
        inputDisabled: bool.isRequired,
    }

    render() {
        return (
            <React.Fragment>
                <TextField
                    id="selectedEan"
                    name="selectedEan"
                    label="Tuotteen EAN"
                    fullWidth
                    value={this.props.ean}
                    onChange={this.props.onChangeActiveProductEan}
                    onKeyUp={this.props.onEanFieldKeyUp}
                    disabled={this.props.inputDisabled}
                />
            </React.Fragment>
        )
    }
}
