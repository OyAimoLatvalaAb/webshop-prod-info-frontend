import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import {
    Button,
} from '@material-ui/core'
import { object, func, bool } from 'prop-types';
import { DeleteSweep, Create, } from '@material-ui/icons';
import ProductInfoProps from '../models/ProductInfoProps';
import { styles } from '../constants';

class ActionButtonsComponent extends Component {

    static propTypes = {
        classes: object.isRequired,
        isNewProduct: bool.isRequired,
        onRequestClearInputForm: func.isRequired,
        onClearInputForm: func.isRequired,
        onLoadProduct: func.isRequired,
        onSaveProduct: func.isRequired,
        productInfo: ProductInfoProps,
        loadButtonActive: bool.isRequired,
        buttonsDisabled: bool.isRequired,
        showClearConfirm: bool.isRequired,
    }
    render() {
        const { classes } = this.props;
        return (
            <div>
                {
                    this.props.showClearConfirm ? (
                        <div>Oletko varma?</div>
                    ) : null
                }
                <div className={classes.buttons}>
                    {
                        this.props.productInfo !== null ?
                            ( this.props.showClearConfirm ? (
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    onClick={this.props.onClearInputForm}
                                    className={classes.button}
                                    disabled={this.props.buttonsDisabled}
                                >
                                    Tallentamattomia muutoksia. Oletko varma?
                                    <DeleteSweep
                                        className={classes.rightIcon}
                                    />
                                </Button>
                            ) :
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    onClick={this.props.onRequestClearInputForm}
                                    className={classes.button}
                                    disabled={this.props.buttonsDisabled}
                                >
                                    Tyhjennä
                                    <DeleteSweep
                                        className={classes.rightIcon}
                                    />
                                </Button>
                            ) : null
                    }
                    {
                        this.props.productInfo !== null ?
                            (
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className={classes.button}
                                    onClick={this.props.onSaveProduct}
                                    disabled={this.props.buttonsDisabled}
                                >
                                    Tallenna
                                    <Create
                                        className={classes.rightIcon}
                                    />
                                </Button>
                            ) : null
                    }
                    {
                        this.props.isNewProduct && this.props.productInfo !== null ? null :
                            (
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className={classes.button}
                                    onClick={this.props.onLoadProduct}
                                    disabled={!this.props.loadButtonActive || this.props.buttonsDisabled}
                                >
                                    Lataa
                                    <Create
                                        className={classes.rightIcon}
                                    />
                                </Button>
                            )
                    }
                </div>
            </div>
        )
    }
}
export default withStyles(styles)(ActionButtonsComponent);
