import React, { Component } from 'react';
import { func, string, bool } from 'prop-types';
import RichTextEditor from 'react-rte';

export default class MyStatefulEditor extends Component {
    static propTypes = {
        html: string,
        setProductText: func,
        readOnly: bool,
        placeholder: string,
    };

    state = {
        // value: RichTextEditor.createEmptyValue()
        value: RichTextEditor.createValueFromString(this.props.html ? this.props.html : '', 'html')
    }

    onChange = (value) => {
        this.setState({value});
        // TODO: Don't call this on every change
        this.props.setProductText(value.toString('html'));
    };

    render () {
        const toolbarConfig = {
            // Optionally specify the groups to display (displayed in the order listed).
            display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
            INLINE_STYLE_BUTTONS: [
                {label: 'Lihavoitu', style: 'BOLD', className: 'custom-css-class'},
                {label: 'Kursivoitu', style: 'ITALIC'},
                {label: 'Alleviivattu', style: 'UNDERLINE'}
            ],
            BLOCK_TYPE_DROPDOWN: [
                {label: 'Tavallinen', style: 'unstyled'},
                {label: 'Suuri Otsikko', style: 'header-one'},
                {label: 'Keskisuuri Otsikko', style: 'header-two'},
                {label: 'Pieni Otsikko', style: 'header-three'}
            ],
            BLOCK_TYPE_BUTTONS: [
                {label: 'Numeroitu Lista', style: 'unordered-list-item'},
                {label: 'Numeroimaton Lista', style: 'ordered-list-item'}
            ]
        };
        return (
            <RichTextEditor
                toolbarConfig={toolbarConfig}
                value={this.state.value}
                onChange={this.onChange}
                readOnly={ typeof this.props.readOnly === 'boolean' ? this.props.readOnly : false }
                placeholder={this.props.placeholder ? this.props.placeholder : ''}
            />
        );
    }
}
