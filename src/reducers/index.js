import _ from 'lodash';
import {
    CLEAR_PRODUCT_INPUT_FORM,
    SET_CATALOG_VISIBILITY,
    SET_DIM_H_CM,
    SET_DIM_W_CM,
    SET_DIM_L_CM,
    SET_MANUFACTURER_WEB_URL,
    SET_MASS_G,
    SET_PROD_TEXT,
    SET_SALE_PRICE,
    SET_UNIT,
    SET_EAN,
    SET_PRODUCT_EAN,
    LOAD_PRODUCT_INFO,
    LOAD_PRODUCT_INFO_FAILED,
    LOAD_PRODUCT_INFO_SUCCEEDED,
    SAVE_PRODUCT_INFO_SUCCEEDED,
    SAVE_PRODUCT_INFO,
    SAVE_PRODUCT_INFO_FAILED,
    CLEAR_NOTIFICATION_MESSAGES,
    ADD_NOTIFICATION_MESSAGE,
    REMOVE_NOTIFICATION_MESSAGE,
    SET_JWT,
    SET_IS_LARGE,
    SET_IS_DANGEROUS,
    SET_IS_FRAGILE,
    SET_VAK_AMOUNT,
    SET_SHOW_CLEAR_PRODUCT_INPUT_FORM_CONFIRM,
} from '../constants/action-types'

import {
    HTTP_NOT_FOUND,
} from '../constants'

import ProductInfo from '../models/ProductInfo';

const savedJWT = localStorage.getItem('JWT');
const initialState = {
    product: {
        ean: '',
        isLoading: false,
        isNew: true,
        isSaving: false,
        error: {
            active: false,
            message: '',
        },
        info: null,
        originalInfo: null,
        isChanged: false,
    },
    requestedClearInputForm: false,
    notificationStack: [],
    JWT: savedJWT,
};

if(!savedJWT && typeof process.env.DEFAULT_JWT === 'string' && process.env.DEFAULT_JWT !== '') {
    initialState.JWT = process.env.DEFAULT_JWT;
}

const parseProductInfoFromAPI = (data) => {
    // XXX: These floats are stored in the DB as numeric (so decimals).
    // But as JS doesn't offer the same resolution as the DB does, they
    // are returned as strings. So hackily casting them here to floats.
    // Realistically, JS precision should do (and the backend is also JS),
    // so not going to use time to verify this now.
    // TODO: Add logic which validates too large numbers
    return {
        catalogVisibility: data.catalogVisibility,
        deferredDelivery: data.deferredDelivery,
        dimHCM: data.heightCm === null ? '' : parseFloat(data.heightCm),
        dimWCM: data.widthCm === null ? '' : parseFloat(data.widthCm),
        dimLCM: data.lengthCm === null ? '' :parseFloat(data.lengthCm),
        manufacturerWebUrl: data.manufacturerWebUrl,
        massG: data.massG === null ? '' : parseFloat(data.massG),
        salePrice: data.salePrice === null ? '' : parseFloat(data.salePrice),
        text: data.text,
        unit: data.unit,
        isLarge: data.isLarge,
        isFragile: data.isFragile,
        isDangerous: data.isDangerous,
        vakAmount: data.vakAmount === null ? '' : parseFloat(data.vakAmount),
    }
}

function rootReducer(state = initialState, action) { // eslint-disable-line
    if(action.type === CLEAR_PRODUCT_INPUT_FORM) {
        const res = {
            ...state,
        }
        res.product.ean = '';
        res.product.info = null;
        res.product.originalInfo = null;
        res.notificationStack = [];
        res.requestedClearInputForm = false;
        res.product.isChanged = false;
        return res;
    } else if(action.type === SET_CATALOG_VISIBILITY ) {
        const res = {
            ...state,
        }
        res.product.info.catalogVisibility = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        res.requestedClearInputForm = false;
        return res;
    } else if(action.type === SET_DIM_H_CM ) {
        const res = {
            ...state,
        }
        res.product.info.dimHCM = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        res.requestedClearInputForm = false;
        return res;
    } else if(action.type === SET_DIM_W_CM ) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.dimWCM = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_DIM_L_CM ) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.dimLCM = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_MANUFACTURER_WEB_URL ) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.manufacturerWebUrl = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_MASS_G ) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.massG = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_PROD_TEXT ) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        if(action.payload === '<p><br></p>') { // Froala 'empty'
            action.payload = ''
        }
        res.product.info.text = action.payload.replace(/&nbsp;/g, " ")
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_SALE_PRICE ) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.salePrice = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_UNIT ) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.unit = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_EAN) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.ean = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_PRODUCT_EAN) {
        const res = {
            ...state,
        }
        res.product.ean = action.payload
        res.product.info = null
        res.product.originalInfo = null
        res.requestedClearInputForm = false;
        return res;
    } else if(action.type === LOAD_PRODUCT_INFO) {
        const res = {
            ...state,
        }
        res.product.error = {
            active: false,
            message: '',
        }
        res.product.isLoading = true
        return res;
    } else if(action.type === LOAD_PRODUCT_INFO_SUCCEEDED) {
        const res = {
            ...state,
        }
        res.product.isLoading = false
        res.product.isNew = false
        res.product.info = parseProductInfoFromAPI(action.payload)
        res.product.originalInfo = Object.assign({}, res.product.info);
        res.requestedClearInputForm = false;
        res.product.error = {
            active: false,
            message: '',
        }
        return res;
    } else if(action.type === LOAD_PRODUCT_INFO_FAILED) {
        const res = {
            ...state,
        }
        res.product.isLoading = false
        if(action.payload.request.status === HTTP_NOT_FOUND) {
            // Got 404, so this product was not found in the db => new prod
            if(state.product.info === null) {
                res.product.info = {...ProductInfo}
                res.product.originalInfo = {...ProductInfo}
            }
            res.product.isNew = true
            res.product.error = {
                active: false,
                message: '',
            }
        } else {
            res.product.error = {
                active: true,
                message: `Virhe: ${action.payload.message}`,
            }
        }
        return res;
    } else if(action.type === SAVE_PRODUCT_INFO) {
        const res = {
            ...state
        }
        res.product.isSaving = true;
        res.product.error.active = false;
        res.product.error.message = '';
        res.notificationStack = [{
            message: 'Tallennetaan tuotteen tietoja...',
            id: (new Date()).getTime(),
        }];
        res.requestedClearInputForm = false;
        return res
    } else if(action.type === SAVE_PRODUCT_INFO_SUCCEEDED) {
        const res = {
            ...state
        }
        res.product.isSaving = false;
        res.product.isNew = false;
        res.product.error.active = false;
        res.product.error.message = '';
        res.notificationStack = [{
            message: 'Tuote tallennettu onnistuneesti.',
            id: (new Date()).getTime(),
        }];
        res.requestedClearInputForm = false;
        return res
    } else if(action.type === SAVE_PRODUCT_INFO_FAILED) {
        const res = {
            ...state
        }
        res.product.isSaving = false;
        res.product.error.active = true;
        res.product.error.message = action.payload;
        // Clear notifiactions
        res.notificationStack = [{
            message: `Virhe: ${action.payload.message}`,
            id: (new Date()).getTime(),
        }];
        return res
    } else if(action.type === ADD_NOTIFICATION_MESSAGE) {
        const res = {
            ...state,
        };
        res.notificationStack.push({
            message: action.payload.message,
            id: action.payload.id,
        });
        return res;
    } else if(action.type === REMOVE_NOTIFICATION_MESSAGE) {
        const res = {
            ...state,
        };
        res.notificationStack = res.notificationStack.filter((elem) => {
            if(elem.id === action.payload.id) {
                return false;
            }
            return true;
        });
        return res;
    } else if(action.type === CLEAR_NOTIFICATION_MESSAGES) {
        const res = {
            ...state,
        };
        res.notificationStack = [];
        return res;
    } else if (action.type === SET_JWT) {
        const res = {
            ...state,
            JWT: action.payload.JWT
        }
        localStorage.setItem('JWT', action.payload.JWT)
        return res;
    } else if(action.type === SET_IS_LARGE) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.isLarge = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_IS_DANGEROUS) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.isDangerous = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_IS_FRAGILE) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.isFragile = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_VAK_AMOUNT) {
        const res = {
            ...state,
        }
        res.requestedClearInputForm = false;
        res.product.info.vakAmount = action.payload
        res.product.isChanged = ! _.isEqual(res.product.info, res.product.originalInfo)
        return res;
    } else if(action.type === SET_SHOW_CLEAR_PRODUCT_INPUT_FORM_CONFIRM) {
        const res = {
            ...state,
            requestedClearInputForm: true
        }
        return res
    }
    return state;
}
export default rootReducer
