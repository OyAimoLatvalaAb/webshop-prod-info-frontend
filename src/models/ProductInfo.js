import { CATALOGVISIBILITY } from '../constants'
const ProductInfo = {
    salePrice: null,
    dimWCM: null,
    dimHCM: null,
    dimLCM: null,
    manufacturerWebUrl: '',
    unit: '',
    massG: null,
    text: '',
    catalogVisibility: CATALOGVISIBILITY.visible,
}

export default ProductInfo;
