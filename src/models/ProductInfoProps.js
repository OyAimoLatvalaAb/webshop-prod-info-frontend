import { shape, number, string, oneOf} from 'prop-types';
import { CATALOGVISIBILITY } from '../constants'
const ProductInfoProps =  shape({
    salePrice: number,
    dimWCM: number,
    dimHCM: number,
    dimLCM: number,
    manufacturerWebUrl: string,
    unit: string,
    massG: number,
    text: string,
    catalogVisibility: oneOf(
        Object.getOwnPropertyNames(CATALOGVISIBILITY).map(
            (k) => CATALOGVISIBILITY[k]
        )
    ),
});

export default ProductInfoProps;
