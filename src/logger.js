// console.log('env:', process.env)
export const logger = (() => {
    if(process.env.ENVIRONMENT === 'PRODUCTION') {
        return {
            error: console.error // eslint-disable-line
        };
    }
    return console;
})()

export default logger;
