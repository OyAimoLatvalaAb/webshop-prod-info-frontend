import { createLogic } from 'redux-logic';
import axios from 'axios';
import _ from 'lodash';
import {
    SET_PRODUCT_DATA,
    DELETE_PRODUCT,
    LOAD_PRODUCT_INFO,
    LOAD_PRODUCT_INFO_SUCCEEDED,
    LOAD_PRODUCT_INFO_FAILED,
    SAVE_PRODUCT_INFO,
    SAVE_PRODUCT_INFO_SUCCEEDED,
    SAVE_PRODUCT_INFO_FAILED,
    REQUEST_CLEAR_INPUT_FORM,
    SET_SHOW_CLEAR_PRODUCT_INPUT_FORM_CONFIRM,
} from '../constants/action-types'
import {
    onClearInputForm,
} from '../actions'
import { logger } from '../logger';

const requestClearInputForm = createLogic({
    type: REQUEST_CLEAR_INPUT_FORM,
    process({ getState }, dispatch, done) {
        const { originalInfo, info } = getState().product
        logger.info('Original', originalInfo, 'current', info)
        if(_.isEqual(info, originalInfo)) {
            logger.info('Product is NOT changed')
            dispatch(onClearInputForm());
        }
        else {
            logger.info('Product is changed')
            dispatch({ type: SET_SHOW_CLEAR_PRODUCT_INPUT_FORM_CONFIRM })
        }
        done()
    }
})

const fetchProduct = createLogic({
    type: LOAD_PRODUCT_INFO,
    process({ getState }, dispatch, done) {
        const { ean } = getState().product
        const { API_URL } = process.env
        axios.get(`${API_URL}product-controller/products/${ean}`, {
            headers: {
                Authorization: "Bearer "+getState().JWT
            }
        }).then(resp =>
            resp.data
        ).then(product =>
            dispatch({type: LOAD_PRODUCT_INFO_SUCCEEDED, payload: product})
        ).catch(err => {
            const action = {type: LOAD_PRODUCT_INFO_FAILED, payload: err}
            dispatch(action)
        }).then(() =>
            done()
        )
    }
});

const saveProductInfo = createLogic({
    type: SAVE_PRODUCT_INFO,
    process({ getState }, dispatch, done) {
        const product = getState().product;
        if(product.isNew) {
            // New product, so POST
            const rawPayload = { ...product.info, EAN: product.ean }
            logger.debug('rawPayload:', rawPayload)
            const payload = _.mapValues(rawPayload, val => val === '' ? null : val)
            logger.debug('payload', payload);
            axios.post(`${process.env.API_URL}product-controller/products`, payload, {
                headers: {
                    Authorization: "Bearer "+getState().JWT
                }
            }).then(product =>
                dispatch({type: SAVE_PRODUCT_INFO_SUCCEEDED, payload: product})
            ).catch(err => {
                dispatch({type: SAVE_PRODUCT_INFO_FAILED, payload: err})
            }).then(() =>
                done()
            )
        } else {
            // Existing product, so PUT
            const rawPayload = product.info
            logger.debug('rawPayload:', rawPayload)
            const payload = _.mapValues(rawPayload, val => val === '' ? null : val)
            logger.debug('payload:', payload)
            axios.put(`${process.env.API_URL}product-controller/products/${product.ean}`, payload, {
                headers: {
                    Authorization: "Bearer "+getState().JWT
                }
            }).then(product =>
                dispatch({type: SAVE_PRODUCT_INFO_SUCCEEDED, payload: product})
            ).catch(err => {
                dispatch({type: SAVE_PRODUCT_INFO_FAILED, payload: err})
            }).then(() =>
                done()
            )
        }
    }
});

const deleteProduct = createLogic({
    type: DELETE_PRODUCT,
    process({ getState, action }, dispatch, done) {
        logger.log(action)
        axios.delete(process.env.API_URL+'product-controller/product/'+getState().product.ean)
            .then(resp => resp.data)
            .then(dispatch({type: SET_PRODUCT_DATA}))
            .catch(err => {
                logger.error(err)
            }).then(() => done())
    }
});

export default [
    fetchProduct,
    saveProductInfo,
    deleteProduct,
    requestClearInputForm,
]
