import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline'
import './index.css'
import { Provider } from 'react-redux'
import store from './store'
import AppContainer from './containers/AppContainer';

ReactDOM.render(
    <Provider store={store}>
        <CssBaseline />
        <AppContainer />
    </Provider>,
    document.getElementById('app')
);
