import { connect } from 'react-redux'
import InputFormComponent from '../components/InputFormComponent'
import {
    onChangeSalePrice,
    onChangeDimWCM,
    onChangeDimHCM,
    onChangeDimLCM,
    onChangeManufacturerWebUrl,
    onChangeUnit,
    onChangeMassG,
    onChangeProdText,
    onChangeCatalogVisibility,
    onChangeIsLarge,
    onChangeIsDangerous,
    onChangeIsFragile,
    onChangeVakAmount,
} from '../actions/'
import { CATALOGVISIBILITY } from '../constants'

const mapDispatchToProps = dispatch => {
    return {
        onChangeSalePrice: (e) => dispatch(onChangeSalePrice(e.target.value)),
        onChangeDimWCM: (e) => dispatch(onChangeDimWCM(e.target.value)),
        onChangeDimHCM: (e) => dispatch(onChangeDimHCM(e.target.value)),
        onChangeDimLCM: (e) => dispatch(onChangeDimLCM(e.target.value)),
        onChangeManufacturerWebUrl: (e) =>
            dispatch(onChangeManufacturerWebUrl(e.target.value)),
        onChangeUnit: (e) => dispatch(onChangeUnit(e.target.value)),
        onChangeMassG: (e) => dispatch(onChangeMassG(e.target.value)),
        onChangeProdText: (html) => dispatch(onChangeProdText(html)),
        onChangeCatalogVisibility: (e) =>
            dispatch(onChangeCatalogVisibility(e.target.value)),
        onChangeIsLarge: (e) => dispatch(onChangeIsLarge(e.target.checked)),
        onChangeIsDangerous: (e) => dispatch(onChangeIsDangerous(e.target.checked)),
        onChangeIsFragile: (e) => dispatch(onChangeIsFragile(e.target.checked)),
        onChangeVakAmount: (e) => dispatch(onChangeVakAmount(e.target.value)),
    }
}

const mapStateToProps = (state, ownProps) => {
    const prodInfo = state.product.info
    const res = {
        ...ownProps,
        ean: state.product.ean,
        isNewProduct: state.product.isNew,
        productIsLoading: state.product.isLoading,
        isBusy: state.product.isLoading || state.product.isSaving,
    }
    if(state.product.info === null) {
        res.salePrice = ''
        res.dimWCM = ''
        res.dimHCM = ''
        res.dimLCM = ''
        res.manufacturerWebUrl = ''
        res.unit = ''
        res.massG = ''
        res.text = ''
        res.catalogVisibility = CATALOGVISIBILITY.visible
        res.isLarge = false
        res.isDangerous = false
        res.isFragile = false
        res.vakAmount = 0.0
    } else {
        res.salePrice = prodInfo.salePrice === null ? '' : prodInfo.salePrice
        res.dimWCM = prodInfo.dimWCM === null ? '' : prodInfo.dimWCM
        res.dimHCM = prodInfo.dimHCM === null ? '' : prodInfo.dimHCM
        res.dimLCM = prodInfo.dimLCM === null ? '' : prodInfo.dimLCM
        res.manufacturerWebUrl = prodInfo.manufacturerWebUrl === null ? '' : prodInfo.manufacturerWebUrl
        res.unit = prodInfo.unit === null ? '' : prodInfo.unit
        res.massG = prodInfo.massG === null ? '' : prodInfo.massG
        res.text = prodInfo.text === null ? '' : prodInfo.text
        res.catalogVisibility = prodInfo.catalogVisibility === null ? CATALOGVISIBILITY.visible : prodInfo.catalogVisibility
        res.isLarge = prodInfo.isLarge === null ? false : prodInfo.isLarge
        res.isDangerous = prodInfo.isDangerous === null ? false : prodInfo.isDangerous
        res.isFragile = prodInfo.isFragile === null ? false : prodInfo.isFragile
        res.vakAmount = prodInfo.vakAmount === null ? 0.0 : prodInfo.vakAmount
    }
    return res
}

export default connect(mapStateToProps, mapDispatchToProps)(InputFormComponent)
