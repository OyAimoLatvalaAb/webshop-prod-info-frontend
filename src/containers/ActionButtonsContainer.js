import { connect } from 'react-redux'
import ActionButtonsComponent from '../components/ActionButtonsComponent'
import {onRequestClearInputForm, onLoadProductInfo, onSaveProductInfo, onClearInputForm} from '../actions'
import { isEan } from '../validators'

const mapDispatchToProps = (dispatch) => {
    return {
        onRequestClearInputForm: () => dispatch(onRequestClearInputForm()),
        onClearInputForm: () => dispatch(onClearInputForm()),
        onLoadProduct: () => dispatch(onLoadProductInfo()),
        onSaveProduct: () => dispatch(onSaveProductInfo()),
    }
}

const mapStateToProps = (state, ownProps) => {
    const res = {
        ...ownProps,
        productInfo: state.product.info,
        isNewProduct: state.product.isNew,
        loadButtonActive: isEan(state.product.ean),
        buttonsDisabled: state.product.isLoading || state.product.isSaving,
        showClearConfirm: state.requestedClearInputForm,
    }
    return res
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionButtonsComponent)
