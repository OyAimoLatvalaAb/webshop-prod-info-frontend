import { connect } from 'react-redux'
import AppStateComponent from '../components/AppStateComponent'
import {isEan} from '../validators'

const mapDispatchToProps = dispatch => { // eslint-disable-line
    return {
    }
}

const mapStateToProps = (state, ownProps) => {
    let statusMsg = ''
    const ean = state.product.ean
    if(state.product.error.active) {
        statusMsg = `Virhe: ${state.product.error.message}`
    } else if(state.product.isLoading) {
        statusMsg = 'Ladataan tuotetta...'
    } else if(state.product.info !== null) { // We have a product
        if(state.product.isNew) {
            statusMsg = `Uusi tuote '${ean}'`
        } else {
            statusMsg = `Muokataan tuotetta '${ean}'`
        }
    } else if(state.product.isSaving) {
        statusMsg = `Tallennetaan tuotetta ${ean}...`
    } else if(ean.length > 0)
        if(isEan(ean)) {
            statusMsg = 'Valmis lataaman tuotetta'
        } else {
            statusMsg = 'Virheellinen EAN'
        }
    else {
        statusMsg = 'Syötä tuotteen EAN'
    }
    return {
        ...ownProps,
        statusMsg,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppStateComponent)
