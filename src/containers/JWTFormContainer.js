import { connect } from 'react-redux'
import JWTFormComponent from '../components/JWTFormComponent'
import {onChangeJWT} from '../actions'

const mapDispatchToProps = dispatch => {
    return {
        onChangeJWT: (e) => dispatch(onChangeJWT(e.target.value)),
    }
}

const mapStateToProps = (state, ownProps) => {
    const res = {
        ...ownProps,
        JWT: state.JWT,
    }
    return res
}

export default connect(mapStateToProps, mapDispatchToProps)(JWTFormComponent)
