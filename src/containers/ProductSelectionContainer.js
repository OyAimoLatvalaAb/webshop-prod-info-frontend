import { connect } from 'react-redux'
import ProductSelectionComponent from '../components/ProductSelectionComponent'
import { onChangeActiveProductEan, onLoadProductInfo, } from '../actions'

const mapDispatchToProps = dispatch => {
    return {
        onChangeActiveProductEan: (e) => dispatch(onChangeActiveProductEan(e.target.value)),
        onEanFieldKeyUp: (e) => {
            if(e.keyCode === 13) /* enter */ {
                dispatch(onLoadProductInfo())
            }
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    const res = {
        ...ownProps,
        ean: state.product.ean,
        inputDisabled: state.product.isChanged,
    }
    return res
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductSelectionComponent)
