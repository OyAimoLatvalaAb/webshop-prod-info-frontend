import { connect } from 'react-redux'
import AppComponent from '../components/AppComponent'

const mapDispatchToProps = dispatch => { // eslint-disable-line
    return {
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        productInfo: state.product.info,
        isNewProduct: state.product.isNew,
        productIsLoading: state.product.isLoading,
        notificationStack: state.notificationStack,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent)
